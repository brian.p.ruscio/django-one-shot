from django.urls import path
from todos.views import todo_list, todo_item

urlpatterns = [
    path("", todo_list, name="todo_list_list"),
    path("<int:id>/", todo_item, name="todo_list_detail")
]
