from django.shortcuts import redirect, render, get_object_or_404
from todos.models import TodoList, TodoItem,
from todos.forms import TodoListForm


# Create your views here.
def todo_list(request):
    item_list = TodoList.objects.all()
    context = {
        "item_list": item_list,
    }
    return render(request, "todos/list.html", context)

def todo_item(request, id):
    list_detail = get_object_or_404(TodoList, id=id)
    context = {
        "todo_list_detail": list_detail,
    }

    return render(request, 'todos/detail.html', context)

def create_list(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todo_list = form.save(False)
            todo_list.save()
            return redirect("todo_list")
    else:
        form = TodoListForm()

    context = {
        "form:" form,
    }

    return render(request, "todos/create.html", context)

#time to submit
